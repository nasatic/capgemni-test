package StepDef;

import com.jayway.jsonpath.JsonPath;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.log4j.Logger;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class ApiStepDef {
    Response response;
    Logger logger = Logger.getLogger("ApiStepDef");

    @Before
    public void setBaseUrl() {
        RestAssured.baseURI = "https://api.citybik.es/v2/networks/";
    }

    @Given("^user can access resource$")
    public void user_can_access_resource() throws Throwable {
        response = given().contentType(ContentType.JSON).when().get("/");
        assertEquals(response.statusCode(), 200);
    }

    @When("^user makes a request$")
    public Object user_makes_a_request() throws Throwable {
        response = when().get("/");
        Object germany = JsonPath.parse(response.asString()).read("$..[?(@.country == 'DE')]");
        return germany;

    }

    @Then("^resource for \"([^\"]*)\" is returned$")
    public void resource_for_is_returned(String sCity) throws Throwable {
        Object allGerman = user_makes_a_request();
        assertTrue(allGerman.toString().contains(sCity));
;
    }

    @Then("^response should contain longitude \"([^\"]*)\" and latitude \"([^\"]*)\"$")
    public void response_should_contain_longitude_and_latitude(String sLong, String sLat) {
        Object frankfurt = JsonPath.parse(response.asString()).read("$..[?(@.city == 'Frankfurt')]");
        System.out.println(frankfurt.toString());
        assertTrue(frankfurt.toString().contains(sLat));
        assertTrue(frankfurt.toString().contains(sLong));
    }

}
