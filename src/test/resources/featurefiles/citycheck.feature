Feature: As a user I want to verify that the city Frankfurt is in Germany and that we can return the city’s corresponding latitude and longitude.

  Background: User have necessary access
    Given user can access resource

  Scenario: Verify city longitude
    When user makes a request
    Then resource for "Frankfurt" is returned
    And response should contain longitude "8.66375" and latitude "50.1072"


